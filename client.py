import sys
import socket
from bitstring import BitArray


def getArgv():

    valid_methods = ('INVITE', 'BYE', 'ACK')

    if len(sys.argv) != 3:
        sys.exit('Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>')
    try:
        if sys.argv[1] in valid_methods:
            method = sys.argv[1]
        else:
            sys.exit('Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>')

        direccion = sys.argv[2]
        direccion = direccion.split(':')
        port = int(direccion[1])
        uri = direccion[0].split('@')
        ip = uri[1]
        receiver = uri[0]

    except AttributeError:
        sys.exit('Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>')

    return method, ip, port, receiver


def createRequest():
    method, ip, port, receiver = getArgv()
    request = None
    if method == 'INVITE':
        request = f'{method} sip:{receiver}@{ip} SIP/2.0\n\nv=0 ' \
                  f'\no=layton@gressenheller.com 127.0.0.1' \
                  f' \ns=CuriousVillageSession \nt=0 \nm=audio {port} RTP'
    elif method == 'BYE':
        request = f'{method} sip:{receiver}@{ip} SIP/2.0'
    elif method == 'ACK':
        request = f'{method} sip:{receiver}@{ip} SIP/2.0\n'

    return request


def main():

    method, ip, port, receiver = getArgv()
    request = createRequest()
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.sendto(request.encode('utf-8'), (ip, port))
            communication = True
            while communication is True:
                if method == 'INVITE':
                    data = my_socket.recv(1024)
                    print(data.decode())
                    responseCode = data.decode().split('\n')[0].split(' ')[1]
                    if responseCode == '200':
                        communication = False
                    elif responseCode == '100':
                        pass
                    elif responseCode == '180':
                        pass
                    else:
                        communication = False
                        print('Error: Not valid response code')
                elif method == 'BYE':
                    data = my_socket.recv(1024)
                    print(data.decode())
                    responseCode = data.decode().split('\n')[0].split(' ')[1]
                    if responseCode == '200':
                        communication = False
                elif method == 'ACK':
                    data = my_socket.recv(1014)
                    info = BitArray(data).bin
                    print(info[0:92])
                    if data == ' ':
                        communication = False

        print("\n\n\nCliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == '__main__':
    main()
